package de.tesis.dynaware.grapheditor.utils;

public enum GraphInputGesture
{

    /**
     * Panning / moving the graph editor viewport
     */
    PAN,

    /**
     * Zooming the graph editor viewport
     */
    ZOOM,

    /**
     * Resizing graph editor elements
     */
    RESIZE,

    /**
     * Moving graph editor elements
     */
    MOVE,

    /**
     * Connecting graph editor elements
     */
    CONNECT,

    /**
     * Selecting graph editor elements
     */
    SELECT;
}
