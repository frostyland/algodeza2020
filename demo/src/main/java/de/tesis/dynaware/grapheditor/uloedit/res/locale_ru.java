package de.tesis.dynaware.grapheditor.uloedit.res;

import java.util.ListResourceBundle;

public class locale_ru extends ListResourceBundle {

    private static Object[][] contents =
            {
                    {"err/locale_tag_empty", "таг локализации пуст"},

                    {"main/key", "ULO редактор"},
                    {"main/com_label", "COM порты"},
                    {"main/menu_file", "_Файл"},
                    {"main/menu_item_sett", "_Настройки"},
                    {"main/menu_item_admin", "_Администрирование"},
                    {"main/menu_item_print_preview", "Просмотр печати..."},
                    {"main/menu_item_exit", "Вы_ход"},
                    {"main/menu_conn", "_Соединение"},
                    {"main/menu_item_conn", "_Соединиться..."},
                    {"main/menu_item_disconn", "_Отключиться"},
                    {"main/menu_item_quick_conn", "_Переподключение"},

                    {"main/menu_mnemo", "_Мнемосхемы"},
                    {"main/menu_item_commands", "_Команды..."},

                    {"main/menu_help", "_Справка"},
                    {"main/menu_locale", "_Язык"},
                    {"main/menu_item_en", "Английский"},
                    {"main/menu_item_ru", "Русский"},
                    {"main/menu_item_be", "Белорусский"},
                    {"main/menu_item_kz", "Казахский"},
                    {"main/menu_item_il", "Иврит"},
                    {"main/menu_item_def", "системный"},
                    {"main/menu_item_rtl", "RTL раскладка"},

                    {"main/menu_item_about", "О программе"},

                    {"log_type/title", "Фильтр журнала"},
                    {"log_type/all", "<все записи>"},
                    {"log_type/info", "информация"},
                    {"log_type/warn", "предупреждения"},
                    {"log_type/err", "ошибки"},

//                    {"main/settings_port_title", "Настройки порта"},
//                    {"main/settings_port_list", "порты"},
//                    {"main/settings_port_baudrate", "скорость"},
//                    {"main/settings_port_databits", "датабиты"},
//                    {"main/settings_port_stopbits", "стопбиты"},
//                    {"main/settings_port_parity", "четность"},
//                    {"main/settings_port_save", "Сохранить"},
//                    {"main/config_title", "Конфигурация..."},
//                    {"main/saving_title", "Сохранение..."},
//                    {"main/tab_bytes_title", "Байты Rx"},
//                    {"main/comm_start", "Порт открыт: %s"},
//                    {"main/comm_stop", "Порт закрыт"},
//
//                    {"mnemo/key", "Мнемосхемы"},
//                    {"mnemo/crane_title", "Кран %s"},
//                    {"mnemo/crane_show", "Показать: "},
//
//                    {"mnemo/crane_anal_v_akb", "V АКБ:"},
//                    {"mnemo/crane_anal_p_dat_1", "P дат.1:"},
//                    {"mnemo/crane_anal_tok_uk", "Ток УК:"},
//                    {"mnemo/crane_anal_r_sol_z", "R сол.закр:"},
//                    {"mnemo/crane_anal_p_dat_2", "P дат.2:"},
//                    {"mnemo/crane_anal_r_sol_o", "R сол.откр:"},
//                    {"mnemo/crane_anal_ur_nes", "SLEEP:"},
//                    {"mnemo/crane_anal_q_sig", "REZERV:"},
//                    {"mnemo/crane_anal_event_0", "RSSI_UK:"},
//                    {"mnemo/crane_anal_delta_t", "LQI:"},
//                    {"mnemo/crane_anal_dbg", "DBG: RSSI_KS"},
//
//                    {"mnemo/crane_diskr_perim", "Периметр:"},
//                    {"mnemo/crane_diskr_online", "Онлайн:"},
//                    {"mnemo/crane_diskr_crane_opened", "Кран открыт:"},
//                    {"mnemo/crane_diskr_cmd_open", "Команда откр:"},
//                    {"mnemo/crane_diskr_crane_closed", "Кран закрыт:"},
//                    {"mnemo/crane_diskr_cmd_close", "Команда закр:"},
//
//                    {"mnemo/crane_btn_open", "Открыть"},
//                    {"mnemo/crane_btn_close", "Закрыть"},
//                    {"mnemo/crane_btn_tele", "Телеметрия"},
//
//                    {"cranes_settings/key", "Настройки по кранам"},
//                    {"cranes_settings/radio_power", "мощность"},
//
//                    {"settings/key", "Настройки"},
//                    {"settings/radio_power", "мощность"},
//
//                    {"settings.port.key", "Настройки порта"},

                    {"test/tag", "ru"}
            };

    @Override
    protected Object[][] getContents() {
        return contents;
    }
}

