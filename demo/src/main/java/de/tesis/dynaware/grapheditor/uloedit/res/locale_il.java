package de.tesis.dynaware.grapheditor.uloedit.res;

import java.util.ListResourceBundle;

public class locale_il extends ListResourceBundle {

    private static Object[][] contents =
            {
                    {"err/locale_tag_empty", "לוקליזציה תג ריק"},

                    //{"main/key", "ULO Editor"},
                    {"main/com_label", "יציאת ה-com"},
                    {"main/menu_file", "קובץ"},
                    {"main/menu_item_sett", "_הגדרות"},
                    {"main/menu_item_admin", "_שורש"},
                    {"main/menu_item_exit", "יציאה"},
                    {"main/menu_conn", "חיבור"},
                    {"main/menu_item_conn", "לחבר..."},
                    {"main/menu_item_disconn", ""},
                    {"main/menu_item_quick_conn", "חיבור מהיר"},

                    {"main/menu_mnemo", "Mnemoschemes"},
                    {"main/menu_item_commands", "פקודות"},

                    {"main/menu_help", "לעזור"},
                    {"main/menu_locale", "שפה"},
                    {"main/menu_item_en", "English"},
                    {"main/menu_item_ru", "Russian"},
                    {"main/menu_item_be", "Belarus"},
                    {"main/menu_item_kz", "Kazakh"},
                    {"main/menu_item_il", "עברית"},
                    {"main/menu_item_def", "system"},
                    {"main/menu_item_rtl", "RTL פריסה"},

                    {"main/menu_item_about", "על התוכנית"},

                    {"log_type/title", "יומן מסנן"},
                    {"log_type/all", "<כל הערכים>"},
                    {"log_type/info", "מידע"},
                    {"log_type/warn", "אזהרות"},
                    {"log_type/err", "שגיאות"},

//                    {"main/settings_port_title", "Port settings"},
//                    {"main/settings_port_list", "ports"},
//                    {"main/settings_port_baudrate", "baud rate"},
//                    {"main/settings_port_databits", "databits"},
//                    {"main/settings_port_stopbits", "stopbits"},
//                    {"main/settings_port_parity", "parity"},
//                    {"main/settings_port_save", "Save"},
//                    {"main/config_title", "Configuring..."},
//                    {"main/saving_title", "Saving..."},
//                    {"main/tab_bytes_title", "Raw bytes"},
//                    {"main/comm_start", "Port is opened: %s"},
//                    {"main/comm_stop", "Port is closed"},
//
//                    {"mnemo/key", "Mnemos"},
//                    {"mnemo/crane_title", "Crane %s"},
//                    {"mnemo/crane_show", "Show: "},
//
//                    {"mnemo/crane_anal_v_akb", "V bat:"},
//                    {"mnemo/crane_anal_p_dat_1", "P dat.1:"},
//                    {"mnemo/crane_anal_tok_uk", "UK curr:"},
//                    {"mnemo/crane_anal_r_sol_z", "R sol.z:"},
//                    {"mnemo/crane_anal_p_dat_2", "P dat.2:"},
//                    {"mnemo/crane_anal_r_sol_o", "R sol.o:"},
//                    {"mnemo/crane_anal_ur_nes", "Level:"},
//                    {"mnemo/crane_anal_q_sig", "Qty:"},
//                    {"mnemo/crane_anal_event_0", "EVENT_0:"},
//                    {"mnemo/crane_anal_delta_t", "DELTA_T:"},
//                    {"mnemo/crane_anal_dbg", "DBG: RSSI_KS"},
//
//                    {"mnemo/crane_diskr_perim", "Perim:"},
//                    {"mnemo/crane_diskr_online", "Online:"},
//                    {"mnemo/crane_diskr_crane_opened", "Crane opened:"},
//                    {"mnemo/crane_diskr_cmd_open", "Cmd open:"},
//                    {"mnemo/crane_diskr_crane_closed", "Crane closed:"},
//                    {"mnemo/crane_diskr_cmd_close", "Cmd close:"},
//
//                    {"mnemo/crane_btn_open", "Open"},
//                    {"mnemo/crane_btn_close", "Close"},
//                    {"mnemo/crane_btn_tele", "Telemetria"},
//
//                    {"cranes_settings/key", "Crane sets"},
//                    {"cranes_settings/radio_power", "power"},
//
//                    {"settings/key", "Settings"},
//                    {"settings/radio_power", "power"},
//
//                    {"settings.port.key", "Port settings"},

                    {"test/tag", "il"}
            };

    @Override
    protected Object[][] getContents() {
        return contents;
    }

}
