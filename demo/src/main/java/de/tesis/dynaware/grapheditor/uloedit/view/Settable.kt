package de.tesis.dynaware.grapheditor.uloedit.view

/**
 * Base interface for base operations
 */
interface Settable {

    fun activate(){}

    fun deactivate(){}

    fun localize(){}

    fun store(){}

    fun restore(){}

    fun toDefault(){}

}