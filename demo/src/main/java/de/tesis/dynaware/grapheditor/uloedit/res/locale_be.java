package de.tesis.dynaware.grapheditor.uloedit.res;

import java.util.ListResourceBundle;

public class locale_be extends ListResourceBundle {

    private static Object[][] contents =
            {
                    {"err/locale_tag_empty", "таг лакалізацыі пусты"},

                    //{"main/key", "ULO Editor"},
                    {"main/com_label", "COM парты"},
                    {"main/menu_file", "Файл"},
                    {"main/menu_item_sett", "Налады"},
                    {"main/menu_item_admin", "Адміністраванне"},
                    {"main/menu_item_exit", "Вы_хад"},
                    {"main/menu_conn", "Злучэнне"},
                    {"main/menu_item_conn", "Злучыцца..."},
                    {"main/menu_item_disconn", "Адключыцца"},
                    {"main/menu_item_quick_conn", "Перападключэнне"},

                    {"main/menu_mnemo", "Мнемосхемы"},
                    {"main/menu_item_commands", "Каманды..."},

                    {"main/menu_help", "Даведка"},
                    {"main/menu_locale", "Мова"},
                    {"main/menu_item_en", "Англійская"},
                    {"main/menu_item_ru", "Руская"},
                    {"main/menu_item_be", "Беларускі"},
                    {"main/menu_item_kz", "Казахская"},
                    {"main/menu_item_il", "Іўрыт"},
                    {"main/menu_item_def", "сістэмная"},
                    {"main/menu_item_rtl", "RTL макет"},

                    {"main/menu_item_about", "Аб праграме"},

                    {"log_type/title", "Фільтр часопіса"},
                    {"log_type/all", "<усе запісы>"},
                    {"log_type/info", "інфармацыя"},
                    {"log_type/warn", "папярэджання"},
                    {"log_type/err", "памылкі"},

//                    {"main/settings_port_title", "Налады порта"},
//                    {"main/settings_port_list", "парты"},
//                    {"main/settings_port_baudrate", "хуткасць"},
//                    {"main/settings_port_databits", "датабиты"},
//                    {"main/settings_port_stopbits", "стопбиты"},
//                    {"main/settings_port_parity", "цотнасць"},
//                    {"main/settings_port_save", "Захаваць"},
//                    {"main/config_title", "Канфігурацыя..."},
//                    {"main/saving_title", "Захаванне..."},
//                    {"main/tab_bytes_title", "Байты Rx"},
//                    {"main/comm_start", "Порт адкрыты: %s"},
//                    {"main/comm_stop", "Порт зачынены"},

                    {"test/tag", "be"}
            };

    @Override
    protected Object[][] getContents() {
        return contents;
    }
}

