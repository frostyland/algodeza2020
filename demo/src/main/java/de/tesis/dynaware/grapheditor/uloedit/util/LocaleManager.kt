package de.tesis.dynaware.grapheditor.uloedit.util

import de.tesis.dynaware.grapheditor.uloedit.app.Const.Locales.LOCALE_KEY
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Locales.L_EN
import de.tesis.dynaware.grapheditor.uloedit.app.myApp
import io.reactivex.subjects.PublishSubject
import java.util.*

/**
 *
 * Что представляет из себя?
 *
 * * Умолчательную локаль/bundle - ту, которая назначена, как список резервных строк в случае отсутствия назначенных
 * * Пользовательскую локаль/бандл - назначенные пользователем через ком.строку/конфигуратор/меню
 * * Системную локаль/бандл - ту, которая назначена системой и работает если не назначено иное
 *
 *
 * Умолчательная вшивается, исходя из соображений архитектуры. В частности, для русскоязычной программы
 * можно назначить русскую локаль, для интернациональной - английскую.
 *
 * Пользовательская назначается при старте через командную строку или конфигуратор, а в процессе работы через меню.
 * При отсутствии соответствующего bundle операция считается неудавшейся, и остается текущий выбор,
 * который зависит от контекста.
 *
 * Системная назначается, если командная строка или конфигуратор не дают корректной информации.
 *
 * **Процесс инициализации:**
 *
 * * список разрешенных тегов известен изначально, согласно разработки
 * * defXXX уже назначены при разработке
 * * sysXXX определяются при инициализации из [Locale.getDefault] etc
 *
 * 1. Поиск ключа командной строки, и попытка настроить userBundle этим ключом. Удача выход, неудача п.2
 * 2. Поиск ключа конфигуратора, и попытка настроить userBundle этим ключом. Удача выход, неудача п.3
 * 3. Назначение userXXX, как sysXXX - выбор системного значения
 *
 * **Процесс переустановки:**
 *
 * 1. Попытка настроить userBundle новым ключом. Удача или нет - событие
 *
 * **Процесс сброса:**

 * 1. userXXX назначаются на defXXX
 *
 *
 * What can be happen with [LocaleManager]?
 *
 *
 *
 * * Initially it sets to system [Locale.getDefault]
 * *
 */
object LocaleManager {

    /**
     * This event of "GoodEvent" type fires when locale has changed.
     */
    data class LocaleChangedEvent(val value: String) : MyEvent

    /**
     * This event of "BadEvent" type fires when such a resource bundle is missing.
     */
    data class MissingResourceEvent(val err: String) : MyEvent

    /**
     * This event of "BadEvent" type fires when resource has no such an entry in the resource bundle.
     */
    data class NoEntryEvent(val err: String) : MyEvent


    val TAGS = listOf("en", "ru", "kz", "be", "il")
    const val PREFIX = "main/menu_item_"
    const val DEF_KEY = "main/menu_item_def"

    private const val LOCALE_PATH = "de.tesis.dynaware.grapheditor.uloedit.res.locale"
    private val ERR_NO_KEY = javaClass.simpleName + " - no such key: "
    private val ERR_NO_DEF_KEY = javaClass.simpleName + " - no such default key: "

    /**
     * EventWrapper that fires when locale has changed.
     */
    val onEvent = PublishSubject.create<EventWrapper<*>>()

    /**
     * Default locale is set by code to handy value.
     * In this case it is set to [L_EN] as international variant.
     * It means that if no locale string were found code will try to find out english one.
     */
    val defLocale: Locale by lazy { Locale.forLanguageTag(L_EN) }

    /**
     * Default bundle keeps appropriate locale strings.
     */
    val defBundle: ResourceBundle by lazy { ResourceBundle.getBundle(LOCALE_PATH, defLocale) }

    /**
     * System locale refers to PC-settings.
     * It used as a start value and as a default when [reset] occurs.
     */
    val sysLocale: Locale by lazy { Locale.getDefault() }

    /**
     * System resource bundle should has strings for system locale.
     * It's obvious that no way to know user system locale for sure,
     * so when fail occurs, we will use [defBundle]
     */
    val sysBundle: ResourceBundle by lazy {
        try {
            ResourceBundle.getBundle(LOCALE_PATH, sysLocale)
        } catch (e: MissingResourceException) {
            defBundle
        }
    }

    /**
     * User locale is set to [sysLocale] by default.
     * Later with setting [userLocale] will be set [userBundle] simultaneously.
     * If some fail occurs during that values stay unchanged.
     */
    var userLocale = sysLocale
        set(value) {
            val prevValue = field
            val prevBundle = userBundle
            val ew = peek(value)
            when(ew) {
                is EventWrapper.Good -> {
                    field = value
                    userBundle = ResourceBundle.getBundle(LOCALE_PATH, field)
                    myApp().cfg.put(LOCALE_KEY, field.language)
                }
                is EventWrapper.Bad -> {
                    field = prevValue
                    userBundle = prevBundle
                }
            }
            onEvent.onNext(ew)
        }

    /**
     * User bundle is set to [sysBundle] by default.
     * Later it is set automatically via [userLocale].
     */
    var userBundle = sysBundle
        private set


    /**
     * Steps for set [userLocale]:
     *
     * 1. try to set from command line
     * 2. if no try to set from config
     * 3. if no set to default
     */
    fun activate() {

        exlog("activate")

        // 1.
        var passed = false
        val tagFromArgs = myApp().args.locale
        if(!tagFromArgs.isNullOrEmpty()) {
            if(peek(Locale.forLanguageTag(tagFromArgs)) is EventWrapper.Good) {
                userLocale = Locale.forLanguageTag(tagFromArgs)
                passed = true
            }
        }

        // 2.
        if(!passed) {
            val tagFromConfig = myApp().cfg.string(LOCALE_KEY).value
            if(!tagFromConfig.isNullOrEmpty()) {
                if(peek(Locale.forLanguageTag(tagFromConfig)) is EventWrapper.Good){
                    userLocale = Locale.forLanguageTag(tagFromConfig)
                    passed = true
                }
            }
        }

        // 3.
        if(!passed) {
            reset()
        }

    }

    /**
     * Just check if such a resource exists. No any assignment happens here.
     */
    fun peek(locale: Locale) : EventWrapper<*> {
        var res: EventWrapper<*> = EventWrapper.Good(LocaleChangedEvent(locale.language))
        try {
            val peekBundle = ResourceBundle.getBundle(LOCALE_PATH, locale)
            if (peekBundle.locale.language != locale.language) {
                throw MissingResourceException("no bundle for ${locale.language}", "", "")
            }
        } catch (e: MissingResourceException) {
            res = EventWrapper.Bad(MissingResourceEvent(e.localizedMessage))
        }
        return res
    }


    /**
     * Getting string by its key.
     * When some problem occurs appropriate [EventWrapper.Bad] is generating:
     *
     * * [ERR_NO_KEY] for the absent entry in the [userBundle]
     * * [ERR_NO_DEF_KEY] for the absent entry in the [defBundle]
     *
     * @return found string as a [Result.Luck] or key as a [Result.Fail].
     */
    fun get(key: String): Result<String> {
        return when (userBundle.containsKey(key)) {
            true -> Result.Luck(userBundle.getString(key))
            false -> {
                onEvent.onNext(EventWrapper.Bad(NoEntryEvent("$ERR_NO_KEY${userLocale.language}_$key")))
                when (defBundle.containsKey(key)) {
                    true -> Result.Luck(defBundle.getString(key))
                    false -> {
                        onEvent.onNext(EventWrapper.Bad(NoEntryEvent("$ERR_NO_DEF_KEY${userLocale.language}_$key")))
                        Result.Fail(key)
                    }
                }
            }
        }
    }

    /**
     * Returns object to initial state.
     */
    fun reset() {
        userLocale = sysLocale
    }

}