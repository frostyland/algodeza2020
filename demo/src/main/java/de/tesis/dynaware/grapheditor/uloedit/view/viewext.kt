package de.tesis.dynaware.grapheditor.uloedit.view

import javafx.scene.Node
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.scene.input.KeyCombination
import tornadofx.*

/**
 * Simple extension for configurate items with standard properties
 */
fun MenuItem.config(): MenuItem {
    isMnemonicParsing = true
    return this
}

/**
 * Simple extension for configurate menus with standard properties
 */
fun Menu.config(): Menu {
    isMnemonicParsing = true
    return this
}


fun MenuBar.myMenu(name: String? = null, graphic: Node? = null, op: Menu.() -> Unit = {}) =
        menu(name, graphic, op).also {
            it.config()
        }

fun Menu.myItem(keyCombination: String, graphic: Node? = null, op: MenuItem.() -> Unit = {}) =
        item("", keyCombination, graphic, op).also {
            it.config()
        }

fun Menu.myItem(keyCombination: KeyCombination? = null, graphic: Node? = null, op: MenuItem.() -> Unit = {}) =
        item("", keyCombination, graphic, op).also {
            it.config()
        }
