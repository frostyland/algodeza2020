package de.tesis.dynaware.grapheditor.uloedit.util

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

/**
 * This function prints message with classname before it.
 */
fun <T : Any> T.exlog(msg: Any?) {
    println("[${this.javaClass.simpleName}] $msg")
}

/**
 * Milliseconds to string.
 */
fun msec2str(time: Long?): String {
    return if (time != null)
        Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDateTime().toString()
    else LocalDateTime.now().toString();
}

fun now() : Long = System.currentTimeMillis()
