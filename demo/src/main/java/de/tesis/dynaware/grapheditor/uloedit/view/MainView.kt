package de.tesis.dynaware.grapheditor.uloedit.view

import de.tesis.dynaware.grapheditor.*
import de.tesis.dynaware.grapheditor.uloedit.util.EventWrapper
import de.tesis.dynaware.grapheditor.uloedit.util.LocaleManager
import de.tesis.dynaware.grapheditor.core.DefaultGraphEditor
import de.tesis.dynaware.grapheditor.core.skins.defaults.connection.SimpleConnectionSkin
import de.tesis.dynaware.grapheditor.core.view.GraphEditorView
import de.tesis.dynaware.grapheditor.demo.GraphEditorPersistence
import de.tesis.dynaware.grapheditor.demo.customskins.*
import de.tesis.dynaware.grapheditor.demo.customskins.tree.TreeConnectorValidator
import de.tesis.dynaware.grapheditor.demo.customskins.ulo.UloNodeSkin
import de.tesis.dynaware.grapheditor.demo.customskins.ulo.UloSkinConstants
import de.tesis.dynaware.grapheditor.demo.selections.SelectionCopier
import de.tesis.dynaware.grapheditor.model.GConnection
import de.tesis.dynaware.grapheditor.model.GNode
import de.tesis.dynaware.grapheditor.model.GraphFactory
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.DEF_HEIGHT
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.DEF_WIDTH
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.DEF_X
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.DEF_Y
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.MAIN_HEIGHT
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.MAIN_POS_X
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.MAIN_POS_Y
import de.tesis.dynaware.grapheditor.uloedit.app.Const.Config.MAIN_WIDTH
import de.tesis.dynaware.grapheditor.uloedit.app.myApp
import de.tesis.dynaware.grapheditor.uloedit.util.putAndSave
import io.reactivex.disposables.CompositeDisposable
import javafx.application.Application
import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.NodeOrientation
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain
import tornadofx.*
import java.io.File
import org.dockfx.DockNode
import org.dockfx.DockPane
import org.dockfx.DockPos

/**
 *
 */
class MainView : View(""), Settable {

    companion object {

        const val DEF_HEIGHT = 600.0
        const val DEF_WIDTH = 800.0
        const val APP_PREFIX = ".uloeditor"

        fun dirExist(dir: String): Boolean {
            val path = userDataDirectory
            return if (!File(path).exists())
                File(path).mkdirs()
            else
                true
        }

        val userDataDirectory: String
            get() = (System.getProperty("user.home") + File.separator + APP_PREFIX + File.separator
                    + applicationVersionString + File.separator)

        val applicationVersionString: String
            get() = "1.0"

    }


    private val TREE_SKIN_STYLESHEET = "treeskins.css" //$NON-NLS-1$
    private val TITLED_SKIN_STYLESHEET = "titledskins.css" //$NON-NLS-1$
    private val ULO_SKIN_STYLESHEET = "uloskins.css" //$NON-NLS-1$

    override val root = BorderPane()

    val ctrl: MainCtrl by inject()

    val genTxt = ArrayList<String>();
    lateinit var ta: TextArea;

    /**
     * Key for config.
     */
    private val RTL_STATE = "rtl"

    /**
     * Keeps flag whether interface is rtl-oriented.
     */
    private var isRtl = false

    private var fl: File? = null;

    private lateinit var tabPane: Node
    //private lateinit var tabPane: TabPane

    private lateinit var dockPane: DockPane

    private lateinit var logPane: Node
    private lateinit var logPaneDockNode: DockNode

    //
    private lateinit var menuFile: Menu
    private lateinit var menuItemLoadFromFile: MenuItem
    private lateinit var miSaveToFile: MenuItem
    private lateinit var miSave: MenuItem
    private lateinit var menuItemExit: MenuItem
    //
    private lateinit var menuEdit: Menu
    private lateinit var miUndo: MenuItem
    private lateinit var miRedo: MenuItem
    private lateinit var miCopy: MenuItem
    private lateinit var miPaste: MenuItem
    private lateinit var miSelectAll: MenuItem
    private lateinit var miDelete: MenuItem
    //
    private lateinit var menuHelp: Menu
    private lateinit var menuItemAbout: MenuItem
    //
    private lateinit var menuComponents: Menu
    private lateinit var miAddNode: MenuItem
    private lateinit var miUloNodeOr: MenuItem
    private lateinit var miUloNodeOr3: MenuItem
    private lateinit var miUloNodeOr2: MenuItem
    private lateinit var miUloNodeAnd: MenuItem
    private lateinit var miUloNodeTr: MenuItem
    private lateinit var miUloNodeTrS: MenuItem
    private lateinit var miUloNodeTrQ: MenuItem
    private lateinit var miAddInputConnector: MenuItem
    private lateinit var miAddOutputConnector: MenuItem
    //
    private lateinit var menuBuild: Menu
    private lateinit var miRawBuild: MenuItem
    //
    private lateinit var menuSkins: Menu
    private lateinit var miDefSkin: MenuItem
    private lateinit var miUloSkin: MenuItem
    private lateinit var miTreeSkin: MenuItem
    private lateinit var miTitledSkin: MenuItem

    private val settables = ArrayList<Settable>()

    //
    private val graphEditorContainer = GraphEditorContainer()
    private val graphEditor = DefaultGraphEditor()
    private val selectionCopier = SelectionCopier(graphEditor.skinLookup, graphEditor.selectionManager)
    private val graphEditorPersistence = GraphEditorPersistence()
    //
    private var defaultSkinController: DefaultSkinController? = null
    private var treeSkinController: TreeSkinController? = null
    private var titledSkinController: TitledSkinController? = null
    private var uloSkinController: UloSkinController? = null
    private val activeSkinController = object : SimpleObjectProperty<SkinController>() {
        override fun invalidated() {
            super.invalidated()
            if (get() != null) {
                get().activate()
            }
        }
    }
    // ~


//    override val root = hbox {
//        label(title) {
//            addClass(Styles.heading)
//        }
//    }

    init {

        val model = GraphFactory.eINSTANCE.createGModel()
        graphEditor.model = model
        graphEditorContainer.setGraphEditor(graphEditor)
        graphEditorContainer.stylesheets.addAll(
                GraphEditorView::class.java.getResource(GraphEditorView.STYLESHEET_VIEW).toExternalForm(),
                GraphEditorView::class.java.getResource(GraphEditorView.GAME_STYLESHEET).toExternalForm(),
                GraphEditorView::class.java.getResource(GraphEditorView.TREE_SKIN_STYLESHEET).toExternalForm(),
                GraphEditorView::class.java.getResource(GraphEditorView.TITLED_SKIN_STYLESHEET).toExternalForm(),
                GraphEditorView::class.java.getResource(GraphEditorView.ULO_SKIN_STYLESHEET).toExternalForm(),
                GraphEditorView::class.java.getResource(GraphEditorView.STYLESHEET_DEFAULTS).toExternalForm())
        setDetouredStyle()

        defaultSkinController = DefaultSkinController(graphEditor, graphEditorContainer)
        treeSkinController = TreeSkinController(graphEditor, graphEditorContainer)
        titledSkinController = TitledSkinController(graphEditor, graphEditorContainer)
        uloSkinController = UloSkinController(graphEditor, graphEditorContainer)

//        activeSkinController.set(defaultSkinController)
        activeSkinController.set(uloSkinController)

        graphEditor.modelProperty().addListener { w, o, n -> selectionCopier.initialize(n) }
        selectionCopier.initialize(model)

        initializeMenuBar()
        addActiveSkinControllerListener()

        with(root) {
            top = buildMenuBar()
            center = buildCenter()
//            center = mainSplit
//            bottom = statusBar.also { bar ->
//                bar.rightItems.add(Separator(Orientation.VERTICAL))
//            }
        }

        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA)
        // initialize the default styles for the dock pane and undocked nodes using the DockFX
        // library's internal Default.css stylesheet
        // unlike other custom control libraries this allows the user to override them globally
        // using the style manager just as they can with internal JavaFX controls
        // this must be called after the primary stage is shown
        // https://bugs.openjdk.java.net/browse/JDK-8132900
        DockPane.initializeDefaultUserAgentStylesheet()

        primaryStage.setOnShowing {
            loadSettings()
            if (dirExist(userDataDirectory)) {
                try {
                    dockPane.loadPreference(userDataDirectory + "dock.pref")
                } catch (e: Exception) {
                    println(e.localizedMessage)
                }
            }

        }

        primaryStage.setOnShown {
            //            loadSettings()
            activate()
        }

        primaryStage.setOnCloseRequest {
            exit()
        }

    }

    override fun activate() {
        settables.forEach { it.activate() }
    }

    override fun deactivate() {
        settables.forEach { it.deactivate() }
        if (dirExist(userDataDirectory))
            dockPane.storePreference(userDataDirectory + "dock.pref")

        // I found no way to automate this inside App itself so do it here.
        myApp().deactivateApp()
    }

    override fun localize() {
        with(myApp().localizer) {
            title = get("main/key").value
            ////--------------------------------------------------------------------------------------------------------
            //menuFile.text = get("main/menu_file").value
            menuFile.text = "Файл"
            menuItemExit.text = LocaleManager.get("main/menu_item_exit").value
            menuItemLoadFromFile.text = "Открыть"
            miSaveToFile.text = "Сохранить в файл..."
            miSave.text = "Сохранить"
            ////--------------------------------------------------------------------------------------------------------
            menuEdit.text = "Редактор"
            miUndo.text = "Отмена"
            miRedo.text = "Повтор"
            miCopy.text = "Копировать"
            miPaste.text = "Вставить"
            miSelectAll.text = "Выделить все"
            miDelete.text = "Удалить"
            ////--------------------------------------------------------------------------------------------------------
            menuComponents.text = "Компоненты"
            miAddNode.text = "+Узел"
            miUloNodeOr.text = "OR (ИЛИ)"
            miUloNodeOr3.text = "OR(3) (ИЛИ + 3 входа)"
            miUloNodeOr2.text = "OR(2) (ИЛИ + 2 входа)"
            miUloNodeAnd.text = "AND (И)"
            miUloNodeTr.text = "Tr (Tr)"
            miUloNodeTrS.text = "TrS (TrS)"
            miUloNodeTrQ.text = "TrQ (TrQ)"
            miAddInputConnector.text = "Добавить вход"
            miAddOutputConnector.text = "Добавить выход"
            ////--------------------------------------------------------------------------------------------------------
            menuBuild.text = "Генераторы"
            miRawBuild.text = "Сырая генерация"
            ////--------------------------------------------------------------------------------------------------------
            menuSkins.text = "Типы диаграмм"
            miDefSkin.text = "По умолч"
            miUloSkin.text = "УЛО"
            miTreeSkin.text = "Дерево"
            miTitledSkin.text = "Заголовки"

        }
    }

    override fun toDefault() {
        primaryStage.x = DEF_X
        primaryStage.y = DEF_Y
        primaryStage.width = DEF_WIDTH
        primaryStage.height = DEF_HEIGHT
        settables.forEach { it.toDefault() }
    }

    /**
     * Calls when it's time to update localization
     */
    fun updateLocale() {
        localize()
    }

    private fun buildCenter(): Node {
        dockPane = DockPane()
        tabPane = graphEditorContainer

        val dockNode = DockNode(tabPane, "MAIN")
        dockNode.setPrefSize(DEF_WIDTH, DEF_HEIGHT * 0.75)
//        println("dockNode.isClosable: ${dockNode.isClosable}")
//        println("dockNode.isMinimizable: ${dockNode.isMinimizable}")
//        println("dockNode.isMinimized: ${dockNode.isMinimized}")
//        println("dockNode.isMaximized: ${dockNode.isMaximized}")
//        println("dockNode.isFloatable: ${dockNode.isFloatable}")

        dockNode.isClosable = false
        dockNode.isFloatable = false
        dockNode.isMaximized = false
        dockNode.isMinimized = false
        dockNode.isMinimizable = false
        dockNode.dockTitleBar = null

        dockNode.dock(dockPane, DockPos.CENTER)

//        println("--------------------")
//        println("dockNode.isClosable: ${dockNode.isClosable}")
//        println("dockNode.isMinimizable: ${dockNode.isMinimizable}")
//        println("dockNode.isMinimized: ${dockNode.isMinimized}")
//        println("dockNode.isMaximized: ${dockNode.isMaximized}")
//        println("dockNode.isFloatable: ${dockNode.isFloatable}")

        logPane = vbox {
            ta = textarea() {
                this.text = genTxt.joinToString(separator = "\n")
            }
        }

        logPaneDockNode = DockNode(logPane)
        logPaneDockNode.setPrefSize(DEF_WIDTH, DEF_HEIGHT * 0.25)
        logPaneDockNode.dock(dockPane, DockPos.BOTTOM)
        logPaneDockNode.closedProperty().addListener { o, oldV, newV ->
            if (newV) {
//                println("o = [${o}], oldV = [${oldV}], newV = [${newV}]")
//                println("logPaneDockNode.isClosed = [${logPaneDockNode.isClosed}]")
                config["dock_position"] = logPaneDockNode.lastDockPos.name;
                config.save();
//                println("pos saved: ${config["dock_position"]}")
            }
        }

        return dockPane
    }


    private fun buildMenuBar(): MenuBar {
        return menubar {

            menuFile = myMenu {
                menuItemLoadFromFile = myItem(keyCombination = "Ctrl+O") {
                    action {
                        graphEditorPersistence.loadFromFile(graphEditor);
                    }
                }
                miSaveToFile = myItem { action { saveToFile(graphEditor) } }
                miSave = myItem { action { save(graphEditor) } }
                separator()
                menuItemExit = myItem(keyCombination = "Alt+X") { action { exit() } }
            }
            menuEdit = myMenu {
                miUndo = myItem(keyCombination = "Ctrl+Z") { action { Commands.undo(graphEditor.model) } }
                miRedo = myItem(keyCombination = "Ctrl+Shift+Z") { action { Commands.redo(graphEditor.model) } }
                separator()
                miCopy = myItem(keyCombination = "Ctrl+C") { action { selectionCopier.copy() } }
                miPaste = myItem(keyCombination = "Ctrl+V") {
                    action {
                        activeSkinController.get().handlePaste(selectionCopier)
                    }
                }
                separator()
                miSelectAll = myItem(keyCombination = "Ctrl+A") { action { activeSkinController.get().handleSelectAll() } }
                miDelete = myItem(keyCombination = "Delete") {
                    action {
                        val selection = ArrayList(graphEditor.selectionManager.selectedItems)
                        graphEditor.delete(selection)
                    }
                }
                myItem {
                    action {
                        //                        println("000000: logPaneDockNode.isClosed = ${logPaneDockNode.isClosed}")
                        if (logPaneDockNode.isClosed) {
                            val l = config["dock_position"].toString();
//                            println("pos restored: $l")
                            logPaneDockNode.closedProperty().value = false
                            logPaneDockNode.dock(dockPane, DockPos.valueOf(l));
                        }
                    }
                }
            }
            menuComponents = myMenu {
                miUloNodeOr = myItem { action { addNodeOr() } }
                miUloNodeOr3 = myItem { action { addNodeOr3() } }
                miUloNodeOr2 = myItem { action { addNodeOr2() } }
                miUloNodeAnd = myItem { action { addNodeAnd() } }
                miUloNodeTr = myItem { action { addNodeTr() } }
                miUloNodeTrS = myItem { action { addNodeTrS() } }
                miUloNodeTrQ = myItem { action { addNodeTrQ() } }
                separator()
                miAddNode = myItem { action { addNode() } }
                separator()
                miAddInputConnector = myItem(keyCombination = "Ctrl+Shift+I") {
                    action {
                        val selection = ArrayList(graphEditor.selectionManager.selectedItems)
                        val uloSkinActive = uloSkinController == activeSkinController.get()
                        if (uloSkinActive) {
                            uloSkinController?.addInput()
                        }
                        selection.forEach { e -> graphEditor.selectionManager.select(e) }
                    }
                }
                miAddOutputConnector = myItem(keyCombination = "Ctrl+Shift+O") {
                    action {
                        val selection = ArrayList(graphEditor.selectionManager.selectedItems)
                        val uloSkinActive = uloSkinController == activeSkinController.get()
                        if (uloSkinActive) {
                            uloSkinController?.addOutput()
                        }
                        selection.forEach { e -> graphEditor.selectionManager.select(e) }
                    }
                }
            }
            menuBuild = myMenu {
                miRawBuild = myItem(keyCombination = "Ctrl+R") {
                    action {
                        ta.text = ""
                        ta.text += "RAW BUILD:\n"
                        ta.text += "addresses:\n"
                        println("RAW BUILD:")
                        println("addresses:")
                        graphEditor.skinLookup.nodeSkins.forEach(fun(e: Map.Entry<GNode, GNodeSkin>) {
                            val n = e.value as UloNodeSkin
                            val t = e.value
                            println("${n.item.id} : ${n.type}")
                            ta.text += "${n.item.id} : ${n.type}\n"
                        });
                        println("connections:")
                        ta.text += "connections:\n"
                        graphEditor.skinLookup.nodeSkins.entries.forEachIndexed { i, e ->
//                            ta.text += "1: $i: ${e.value.item.id}\n"
                            val n = e.value as UloNodeSkin
                            n.rightConnectorSkins.forEachIndexed { i2, e2 ->
                                if(e2.item.connections.size > 0) {
                                    ta.text += "${n.item.id} : $i2 : ${e2.item.connections[0]?.target?.parent?.id}\n"
                                }
                            }
                        }


//                        graphEditor.skinLookup.nodeSkins.forEach(fun(e: Map.Entry<GNode, GNodeSkin>) {
//                            val n = e.value as UloNodeSkin
//                            println("n #${n.item.id}")
//                            n.rightConnectorSkins.forEachIndexed(fun(index: Int, ctr: GConnectorSkin) {
//                                println("ctr #${ctr.item.id} : ${index}")
//                                ctr.item.connections.forEachIndexed(fun(i2: Int, cnn: GConnection) {
//                                    val s = "${e.key.id} : ${i2} : ${cnn.target.parent.id}";
//                                    println("$s")
//                                    ta.text += "$s\n"
//                                })
//                            });
//                        });
                    }
                }
            }
            menuSkins = myMenu {
                miDefSkin = myItem {
                    action {
                        setDefaultSkin()
                    }
                }
                miUloSkin = myItem {
                    action {
                        setUloSkin()
                    }
                }
                miTreeSkin = myItem {
                    action {
                        setTreeSkin()
                    }
                }
                miTitledSkin = myItem {
                    action {
                        setTitledSkin()
                    }
                }
            }


        }
    }

    private fun loadSettings() {

        isRtl = myApp().cfg.boolean(RTL_STATE).value?.toBoolean() ?: false

        root.nodeOrientation = if (isRtl) {
            NodeOrientation.RIGHT_TO_LEFT
        } else {
            NodeOrientation.LEFT_TO_RIGHT
        }

        with(myApp().cfg) {
            primaryStage.x = double(MAIN_POS_X).value?.toDouble() ?: DEF_X
            primaryStage.y = double(MAIN_POS_Y).value?.toDouble() ?: DEF_Y
            primaryStage.width = double(MAIN_WIDTH).value?.toDouble() ?: DEF_WIDTH
            primaryStage.height = double(MAIN_HEIGHT).value?.toDouble() ?: DEF_HEIGHT
        }



        localize()
    }

    private fun saveSettings() {
        myApp().cfg.putAndSave {
            put(MAIN_POS_X, primaryStage.x)
            put(MAIN_POS_Y, primaryStage.y)
            put(MAIN_WIDTH, primaryStage.width)
            put(MAIN_HEIGHT, primaryStage.height)
        }
    }

    private fun setDetouredStyle() {
        val customProperties = graphEditor.properties.customProperties
        customProperties[SimpleConnectionSkin.SHOW_DETOURS_KEY] = java.lang.Boolean.toString(true)
        graphEditor.reload()
    }

    private fun initializeMenuBar() {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun addActiveSkinControllerListener() {
        activeSkinController.addListener { o, ov, nv -> handleActiveSkinControllerChange() }
    }

    private val STYLE_CLASS_TITLED_SKINS = "titled-skins" //$NON-NLS-1$

    private fun handleActiveSkinControllerChange() {
        if (treeSkinController == activeSkinController.get()) {
            graphEditor.setConnectorValidator(TreeConnectorValidator())
            graphEditor.view.styleClass.remove(STYLE_CLASS_TITLED_SKINS)
        } else if (titledSkinController == activeSkinController.get()) {
            graphEditor.setConnectorValidator(null)
            if (!graphEditor.view.styleClass.contains(STYLE_CLASS_TITLED_SKINS)) {
                graphEditor.view.styleClass.add(STYLE_CLASS_TITLED_SKINS)
            }
        } else if (uloSkinController == activeSkinController.get()) {
        } else {
            graphEditor.setConnectorValidator(null)
            graphEditor.view.styleClass.remove(STYLE_CLASS_TITLED_SKINS)
        }
        selectionCopier.clearMemory()

        //
        clearAll()
        flushCommandStack()
        checkConnectorButtonsToDisable()
    }


    fun clearAll() {
        Commands.clear(graphEditor.model)
    }

    private fun flushCommandStack() {
        val editingDomain = AdapterFactoryEditingDomain.getEditingDomainFor(graphEditor.model)
        editingDomain?.commandStack?.flush()
    }

    private fun checkConnectorButtonsToDisable() {
        val nothingSelected = graphEditor.selectionManager.selectedItems.stream()
                .noneMatch { e -> e is GNode }
        val treeSkinActive = treeSkinController == activeSkinController.get()
        val titledSkinActive = titledSkinController == activeSkinController.get()
        val uloSkinActive = uloSkinController == activeSkinController.get()

        if (titledSkinActive || treeSkinActive) {
        } else if (nothingSelected) {
        } else {
        }

//        intersectionStyle.setDisable(treeSkinActive)
    }

    fun addNode() {
        activeSkinController.get().addNode(graphEditor.view.localToSceneTransform.mxx)
    }

    private fun addNodeOr() {
        if (activeSkinController.get() is UloSkinController) {
            val c = activeSkinController.get() as UloSkinController
            c.nodeType = UloSkinConstants.ULO_NODE_OR
            c.addNode(graphEditor.view.localToSceneTransform.mxx)
        } else {
            println("GraphEditorDemoController.addNodeOr WRONG SkinController")
        }
    }

    private fun addNodeOr3() {
        if (activeSkinController.get() is UloSkinController) {
            val c = activeSkinController.get() as UloSkinController
            c.nodeType = UloSkinConstants.ULO_NODE_OR_3
            c.addNode(graphEditor.view.localToSceneTransform.mxx)
        } else {
            println("GraphEditorDemoController.addNodeOr3 WRONG SkinController")
        }
    }

    private fun addNodeOr2() {
        if (activeSkinController.get() is UloSkinController) {
            val c = activeSkinController.get() as UloSkinController
            c.nodeType = UloSkinConstants.ULO_NODE_OR_2
            c.addNode(graphEditor.view.localToSceneTransform.mxx)
        } else {
            println("GraphEditorDemoController.addNodeOr2 WRONG SkinController")
        }
    }

    fun addNodeAnd() {
        if (activeSkinController.get() is UloSkinController) {
            val c = activeSkinController.get() as UloSkinController
            c.nodeType = UloSkinConstants.ULO_NODE_AND
            c.addNode(graphEditor.view.localToSceneTransform.mxx)
        } else {
            println("GraphEditorDemoController.addNodeAnd WRONG SkinController")
        }
    }

    fun addNodeTr() {
        if (activeSkinController.get() is UloSkinController) {
            val c = activeSkinController.get() as UloSkinController
            c.nodeType = UloSkinConstants.ULO_NODE_TR
            c.addNode(graphEditor.view.localToSceneTransform.mxx)
        } else {
            println("GraphEditorDemoController.addNodeTr WRONG SkinController")
        }
    }

    fun addNodeTrS() {
        if (activeSkinController.get() is UloSkinController) {
            val c = activeSkinController.get() as UloSkinController
            c.nodeType = UloSkinConstants.ULO_NODE_TR_S
            c.addNode(graphEditor.view.localToSceneTransform.mxx)
        } else {
            println("GraphEditorDemoController.addNodeTrS WRONG SkinController")
        }
    }

    fun addNodeTrQ() {
        if (activeSkinController.get() is UloSkinController) {
            val c = activeSkinController.get() as UloSkinController
            c.nodeType = UloSkinConstants.ULO_NODE_TR_Q
            c.addNode(graphEditor.view.localToSceneTransform.mxx)
        } else {
            println("GraphEditorDemoController.addNodeTrS WRONG SkinController")
        }
    }

    fun setDefaultSkin() {
        activeSkinController.set(defaultSkinController)
        println("setDefaultSkin")
    }

    fun setTreeSkin() {
        activeSkinController.set(treeSkinController)
    }

    fun setTitledSkin() {
        activeSkinController.set(titledSkinController)
    }

    fun setUloSkin() {
        activeSkinController.set(uloSkinController)
    }

    private fun saveToFile(graphEditor: GraphEditor) {

        val scene = graphEditor.view.scene

        if (scene != null) {
            val file = graphEditorPersistence.showFileChooser(scene.window, true)
            fl = file
            if (file != null && graphEditor.model != null) {
                graphEditorPersistence.saveModel(file, graphEditor.model)
            }
        }
    }

    private fun save(graphEditor: GraphEditor) {
        if (fl != null && graphEditor.model != null) {
            graphEditorPersistence.saveModel(fl, graphEditor.model)
        }
    }


    private fun exit() {
        saveSettings()
        deactivate()
        Platform.exit()
    }


}

/**
 *
 */
class MainCtrl : Controller(), Settable {

    val view: MainView by inject()

    val dsp = CompositeDisposable()

    override fun activate() {
        dsp.add(LocaleManager.onEvent.subscribe {
            if (it is EventWrapper.Good && it.ev is LocaleManager.LocaleChangedEvent) {
                view.updateLocale()
            }
        })

    }

    override fun deactivate() {
        dsp.clear()
    }

}