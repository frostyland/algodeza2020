package de.tesis.dynaware.grapheditor.uloedit.app

import com.beust.jcommander.JCommander
import com.beust.jcommander.Parameter
import de.tesis.dynaware.grapheditor.uloedit.util.LocaleManager
import de.tesis.dynaware.grapheditor.uloedit.util.MyLog
import de.tesis.dynaware.grapheditor.uloedit.util.SystemConfig
import de.tesis.dynaware.grapheditor.uloedit.util.User
import de.tesis.dynaware.grapheditor.uloedit.view.MainView
import javafx.application.Application
import tornadofx.App

/**
 * Shortens [KommateApp] instance invoking.
 */
fun myApp(): MyApp {
    return myApp
}

class MyApp: App(MainView::class, Styles::class){

    /**
     * Keeps available command-line arguments.
     */
    val args: MyArgs by lazy { MyArgs }

    /**
     * Used for localization.
     */
    val localizer: LocaleManager by lazy {
        LocaleManager.also {
            it.activate()
        }
    }

    val cfg: SystemConfig by lazy { SystemConfig() }

    /**
     * log-object, used for write events message down.
     */
    val log: MyLog by lazy {
        MyLog.also {
            it.activate()
        }
    }

    var user: User? = null

    init {
        myApp = this

        // These strings need to instantiate objects on time.
        cfg.also { systemConfig ->
            if (MyArgs.resetAll) {
                systemConfig.clear()
            }
            if (MyArgs.resetGeom) {
                systemConfig.keys().toList().filter {
                    it.toString().startsWith(Const.Config.GEOM)
                }.map {
                    systemConfig.remove(it.toString())
                }
            }
        }
        log
        localizer
    }

    fun deactivateApp() {
        log.info("KommateApp.deactivateApp", System.currentTimeMillis())
        log.deactivate()
    }

}

internal lateinit var myApp: MyApp

/**
 * Command-line arguments object-helper. Using this and [JCommander] library parsing is processed.
 *
 *
 */
object MyArgs {

    @Parameter(names = ["--locale", "-l"], description = "Localization, e.g: -l \"en\" or --locale \"kz\"")
    var locale: String? = null

    @Parameter(names = ["--reset-geom", "-r"], description = "Resets only window's geometry")
    var resetGeom: Boolean = false

    @Parameter(names = ["--reset-all", "-R"], description = "Resets all settings")
    var resetAll: Boolean = false

    /**
     * This key shows help on command-line parameters.
     */
    @Parameter(names = ["--help, -h"], description = "Help for command-line parameters", help = true)
    var help: Boolean = false

}


/**
 * Method for initial arguments parsing.
 */
fun parseArgs(argv: Array<String>) {
    // Thanks to [MyArgs] in this CTR args constructs first.
    JCommander(MyArgs).also {
        try {
            it.parse(*argv)
        } catch (e: Exception) {
            println("Error in parameters: ${e.localizedMessage}\n\n")
            it.usage()
        }
    }
}

/**
 * Main function.
 */
fun main(argv: Array<String>) {
    parseArgs(argv);
    Application.launch(MyApp::class.java, *argv)
}
