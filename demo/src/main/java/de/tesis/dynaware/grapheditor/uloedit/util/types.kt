package de.tesis.dynaware.grapheditor.uloedit.util

//==========================================

/**
 * Base class to avoid exceptions via "type-sum" concept
 */
sealed class Result<out V>(val value: V) {

    /**
     * Equals "true" result with value of type V
     */
    class Luck<out V>(value: V) : Result<V>(value)
    /**
     * Equals "fail" result with fail of type V (string message or special "fail value")
     */
    class Fail<out V>(value: V) : Result<V>(value)

}


//==========================================

/**
 * Base class to avoid exceptions via "type-sum" concept when keeping and passing value.
 */
sealed class Value<out V, out T> {

    /**
     * This means that value is correct.
     */
    open class Ok<out V>(val value: V) : Value<V, Nothing>()

    /**
     * This means that value is incorrect.
     */
    open class Err<out V>(val err: V) : Value<Nothing, V>()

}



//==========================================

/**
 * This is concept of two event types - Good (when all is ok) and Bad (when something goes wrong).
 * It helps split all possible events into 2 streams for logic reactions on them.
 *
 * Samples:
 * * Firing good event: EventWrapper.Good(LocaleChangedEvent(locale.language))
 * * Firing bad event: EventWrapper.Bad(MissingResourceEvent(e.localizedMessage))
 */
sealed class EventWrapper<out Type> {

    /**
     * Create this object when wanna give a sign that things go well.
     * Pass instance of real event as a parameter [ev]
     */
    data class Good<out GoodEvent : MyEvent>(val ev: GoodEvent) : EventWrapper<GoodEvent>()

    /**
     * Create this event when wanna give a sign that things are broken (in catch{} for example).
     * Pass instance of real event as a parameter [ev]
     */
    data class Bad<out BadEvent : MyEvent>(val ev: BadEvent) : EventWrapper<BadEvent>()

}

/**
 * Marks descendants as an Event type.
 */
interface MyEvent

