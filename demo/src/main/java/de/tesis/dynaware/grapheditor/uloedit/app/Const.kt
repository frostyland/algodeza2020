package de.tesis.dynaware.grapheditor.uloedit.app

/**
 * Constants for kommate.
 */
object Const {

    const val GUEST_ID = "guest_id"

    /**
     * Keys and defaults for configuration.
     */
    object Config {

        const val POS_X = "pos_x"
        const val POS_Y = "pos_y"
        const val WIDTH = "width"
        const val HEIGHT = "height"

        const val DEF_X = 10.0
        const val DEF_Y = 10.0
        const val DEF_WIDTH = 900.0
        const val DEF_HEIGHT = 600.0
        const val DEF_SPLIT = 0.7

        const val GEOM = "geom/"

        const val MAIN_WIN_NAME = "${GEOM}main_view"

        const val MAIN_POS_X = "$MAIN_WIN_NAME.$POS_X"
        const val MAIN_POS_Y = "$MAIN_WIN_NAME.$POS_Y"
        const val MAIN_WIDTH = "$MAIN_WIN_NAME.$WIDTH"
        const val MAIN_HEIGHT = "$MAIN_WIN_NAME.$HEIGHT"
        const val MAIN_SPLIT = "$MAIN_WIN_NAME.msp"

    }

    object Locales {
        const val LOCALE_KEY = "locale"

        const val L_EN = "EN"
        const val L_RU = "RU"
        const val L_KZ = "KZ"
        const val L_BE = "BE"
        const val L_IL = "IL"
        const val L_OL = "OLBAN"
        const val L_DEF = ""
    }

}
