package de.tesis.dynaware.grapheditor.uloedit.util

import de.tesis.dynaware.grapheditor.uloedit.util.MyLog.MsgType.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

/**
 *
 */
object MyLog {

    enum class MsgType {INFO, WARN, ERR}

    data class Msg(val str: String, val time: Long, val type: MsgType)

    val items = ArrayList<Msg>()

    /**
     * Event of info message.
     */
    data class Event(val msg: Msg) : MyEvent
    //data class InfoEvent(val msg: Msg) : MyEvent

    val onEvent = PublishSubject.create<EventWrapper<*>>()

    private val dsp = CompositeDisposable()

    fun info(msg: String, time: Long? = null) = process(msg, time, INFO)

    fun warn(msg: String, time: Long? = null) = process(msg, time, WARN)

    fun err(msg: String, time: Long? = null) = process(msg, time, ERR)

    fun activate() {
        info("log.activate")
        items.clear()
    }

    fun deactivate() {
        info("log.deactivate")
        items.clear()
    }

    private fun process(msg: String, time: Long? = null, type: MsgType) =
        Msg(msg, time ?: System.currentTimeMillis(), type).also {
            items.add(it)
            onEvent.onNext(EventWrapper.Good(Event(it)))
        }

}

