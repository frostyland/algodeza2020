/*
 * Copyright (C) 2005 - 2014 by TESIS DYNAware GmbH
 */
package de.tesis.dynaware.grapheditor.demo.customskins.ulo;

/**
 * Some constants used by the tree skins.
 */
public class UloSkinConstants {

    public static final String ULO_NODE_PREFIX = "ulo-node"; //$NON-NLS-1$
    public static final String ULO_NODE_OR = "ulo-node-or"; //$NON-NLS-1$
    public static final String ULO_NODE_OR_3 = "ulo-node-or_3"; //$NON-NLS-1$
    public static final String ULO_NODE_OR_2 = "ulo-node-or_2"; //$NON-NLS-1$
    public static final String ULO_NODE_AND = "ulo-node-and"; //$NON-NLS-1$
    public static final String ULO_NODE_TR = "ulo-node-tr"; //$NON-NLS-1$
    public static final String ULO_NODE_TR_S = "ulo-node-tr-s"; //$NON-NLS-1$
    public static final String ULO_NODE_TR_Q = "ulo-node-tr-q"; //$NON-NLS-1$
    public static final String ULO_INPUT_CONNECTOR = "ulo-input"; //$NON-NLS-1$
    public static final String ULO_OUTPUT_CONNECTOR = "ulo-output"; //$NON-NLS-1$
    public static final String TREE_CONNECTION = "tree-connection"; //$NON-NLS-1$
}
