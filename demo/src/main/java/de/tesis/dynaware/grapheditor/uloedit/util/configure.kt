package de.tesis.dynaware.grapheditor.uloedit.util

import com.squareup.moshi.Json
import com.squareup.moshi.Moshi
import de.tesis.dynaware.grapheditor.uloedit.app.Const
import de.tesis.dynaware.grapheditor.uloedit.app.myApp
import java.io.IOException
import java.util.*


/**
 * Entry in the config.
 *
 * [key] key in configuration.
 * [value] value for this key
 * [created] time when this record was created
 * [edited] time when this record was edited
 */
data class ConfigItem(val key: String,
                      var value: String?,
                      val created: Long,
                      var edited: Long,
                      @Json(name = "user_id") val userId: String)


interface Configuration {

    fun put(key: String, value: String?, autoSave: Boolean = false)
    fun put(key: String, value: Boolean?, autoSave: Boolean = false)
    fun put(key: String, value: Double?, autoSave: Boolean = false)
    fun put(key: String, value: Int?, autoSave: Boolean = false)
    fun remove(key: String)

    fun string(key: String): ConfigItem
    fun boolean(key: String): ConfigItem
    fun double(key: String): ConfigItem
    fun int(key: String): ConfigItem

    fun save()
    fun clear()
    fun backup() : Map<String, String>
    fun restore(backup: Map<String, String>)
    fun keys() : Set<*>
}

data class SystemConfig(val path: String = "") : Configuration {

    private val moshi = Moshi.Builder().build().adapter(ConfigItem::class.java)
    private val cfg = myApp().config

    // todo 18/08/23 проверять сброс настроек - на растягивание лога. на комбобокс типов логов

    override fun put(key: String, value: String?, autoSave: Boolean) {
        val ci = get(key).also {
            it.edited = now()
            it.value = value
        }
        val prefix = "$path/${myApp().user?.id ?: Const.GUEST_ID}"
        val fullKey = if(key.startsWith(prefix)) key else "$prefix/$key"
        cfg[fullKey] = moshi.toJson(ci)
        if(autoSave){
            cfg.save()
        }
    }

    override fun put(key: String, value: Boolean?, autoSave: Boolean) {
        put(key, value.toString(), autoSave)
    }

    override fun put(key: String, value: Double?, autoSave: Boolean) {
        put(key, value.toString(), autoSave)
    }

    override fun put(key: String, value: Int?, autoSave: Boolean) {
        put(key, value.toString(), autoSave)
    }

    override fun remove(key: String) {
        val userId = myApp().user?.id ?: Const.GUEST_ID
        val fullKey = "$path/$userId/$key"
        myApp().config.keys.remove(fullKey)
    }

    override fun string(key: String): ConfigItem {
        return get(key)
    }

    override fun boolean(key: String): ConfigItem {
        return get(key)
    }

    override fun double(key: String): ConfigItem {
        return get(key)
    }

    override fun int(key: String): ConfigItem {
        return get(key)
    }

    override fun save() {
        cfg.save()
    }

    override fun clear() {
        // TODO чистить только для пользователя!
        val userId = myApp().user?.id ?: Const.GUEST_ID
        val fullKey = "$path/$userId"

        myApp().config.clear()
    }

    override fun backup(): Map<String, String> {
        val backup = HashMap<String, String>()
        myApp().config.entries.forEach {
            backup[it.key.toString()] = it.value.toString()
        }
        return backup
    }

    override fun restore(backup: Map<String, String>) {
        clear()
        backup.forEach {
            myApp().config[it.key] = it.value
        }
    }

    /**
     * Check if exists and get. If not - get null in [ConfigItem.value]
     */
    private fun get(key: String): ConfigItem {
        val userId = myApp().user?.id ?: Const.GUEST_ID
        val fullKey = "$path/$userId/$key"
        val json = cfg.string(fullKey, "no any json here")
        return try {
            moshi.fromJson(json)
        } catch (e: IOException) {
            null
        } ?: ConfigItem(key, null, now(), now(), userId)
    }

    // TODO 18/08/23 Тестировать возврат ключей по юзеру
    override fun keys(): Set<*> {
        val userId = myApp().user?.id ?: Const.GUEST_ID
        val set = HashSet<String>()
        myApp().config.keys.filter {
            it.toString().startsWith("$path/$userId/")
        }.toSet().forEach {
            set.add(it.toString().substringAfter("$path/$userId/"))
        }
        return set
    }

}

fun SystemConfig.putAndSave(op: SystemConfig.() -> Unit) {
    op().also {
        myApp().config.save()
    }
}
