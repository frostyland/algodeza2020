package de.tesis.dynaware.grapheditor.uloedit.util

data class User(val id: String, val nick: String)