package de.tesis.dynaware.grapheditor.uloedit.res;

import java.util.ListResourceBundle;

public class locale_kz extends ListResourceBundle {

    private static Object[][] contents =
            {
                    {"err/locale_tag_empty", "таг оқшаулау бос"},

                    //{"main/key", "ULO Editor"},
                    {"main/com_label", "COM порттары"},
                    {"main/menu_file", "Файл"},
                    {"main/menu_item_sett", "Параметрлер"},
                    {"main/menu_item_admin", "Администрирование"},
                    {"main/menu_item_exit", "Аяқ_тау"},
                    {"main/menu_conn", "Біріктіру"},
                    {"main/menu_item_conn", "Байланысу..."},
                    {"main/menu_item_disconn", "Отключиться"},
                    {"main/menu_item_quick_conn", "Жылдам қосылу"},

                    {"main/menu_mnemo", "Мнемосхемы"},
                    {"main/menu_item_commands", "Команда..."},

                    {"main/menu_help", "Анықтама"},
                    {"main/menu_locale", "Тілі"},
                    {"main/menu_item_en", "Ағылшын"},
                    {"main/menu_item_ru", "Орыс"},
                    {"main/menu_item_be", "Беларусь"},
                    {"main/menu_item_kz", "Қазақ"},
                    {"main/menu_item_il", "Иврит"},
                    {"main/menu_item_def", "жүйелі"},
                    {"main/menu_item_rtl", "RTL mакеті"},

                    {"main/menu_item_about", "Бағдарламасы туралы"},

                    {"log_type/title", "Сүзгі журналы"},
                    {"log_type/all", "<барлық жазбалар>"},
                    {"log_type/info", "ақпарат"},
                    {"log_type/warn", "алдын алу"},
                    {"log_type/err", "қателер"},

//                    {"main/settings_port_title", "Параметрлер порт"},
//                    {"main/settings_port_list", "порттары"},
//                    {"main/settings_port_baudrate", "жылдамдығы"},
//                    {"main/settings_port_databits", "датабиты"},
//                    {"main/settings_port_stopbits", "стопбиты"},
//                    {"main/settings_port_parity", "жұптық"},
//                    {"main/settings_port_save", "Сақтау"},
//                    {"main/config_title", "Конфигурациясы..."},
//                    {"main/saving_title", "Сақтау..."},
//                    {"main/tab_bytes_title", "Байттар Rx"},
//                    {"main/comm_start", "Порт ашылды: %s"},
//                    {"main/comm_stop", "Порт жабылды"},
//
//                    {"mnemo/key", "Мнемосхемы"},
//                    {"mnemo/crane_title", "Кран %s"},
//                    {"mnemo/crane_show", "Көрсету: "},

                    {"test/tag", "kz"}
            };

    @Override
    protected Object[][] getContents() {
        return contents;
    }

}
